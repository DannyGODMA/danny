<?php
require 'app/Models/Conexion.php';
require 'app/Models/Productos.php';
use controlInventario\Productos;
use controlInventario\Conexion;

class ProductosController
{

    function vistaAgregar(){
        require "app/Views/vistaAgregar.php";
    }
    function vistaActualizar(){
        require "app/Views/vistaActualizar.php";
    }
    function vistaEliminar(){
        require "app/Views/vistaEliminar.php";
    }
    function vistaMostrar(){
        require "app/Views/vistaMostrar.php";
    }

    function registrarProductos(){
        $productos=new Productos();
        $productos->nombre=$_POST["nombre"];
        $productos->cantidad=$_POST["cantidad"];
        $productos->costo=$_POST["costo"];
        $productos->caducidad=$_POST["caducidad"];
        $productos->registrar();

    }
    function actualizarProductos(){
        $productos=new Productos();
        $productos->cantidad=$_POST["cantidad"];
        $productos->costo=$_POST["costo"];
        $productos->idProducto=$_POST["idProducto"];
        $productos->actualizar();
    }
    /*
    //verificar existencia de producto para actualizar
    function verificarExistencias1(){
        if((!isset($_POST["idProducto"]) )){
            echo "No existe el producto";
            return false;
        }
        $idProducto=$_POST["idProducto"];
        $verificar=Productos::verificarexistencia1();
        if($verificar){
            $producto=new Productos();
            $producto->cantidad=$_POST["cantidad"];
            $producto->costo=$_POST["costo"];
            $producto->idProducto=$_POST["idProducto"];
            $producto->actualizar();

        }else{
            $estatus="DATOS NO EXISTENTES";
            require "app/Views/vistaActualizar.php";
        }
    }*/

    //verificar existencia de producto para eliminar
    function verificarExistencias(){
        if((!isset($_POST["nombre"]) || (!isset($_POST["idProducto"])))){
            echo "No existe el producto";
            return false;
        }
        $nombre=$_POST["nombre"];
        $idProducto=$_POST["idProducto"];
        $verificar=Productos::verificarexistencia($nombre,$idProducto);
        if($verificar){
            $producto=new Productos();
            $producto->eliminar($_POST["nombre"], $_POST["idProducto"]);

        }else{
            $estatus="DATOS NO EXISTENTES";
            require "app/Views/vistaEliminar.php";
        }
    }


}