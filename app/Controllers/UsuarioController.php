<?php
require 'app/Models/Conexion.php';
require 'app/Models/Usuario.php';
use controlInventario\Usuario;
use controlInventario\Conexion;


class UsuarioController
{

    function vistaUsuarios(){
        require "app/Views/vistaUsuarios.php";
    }

    function registro(){
        $usuario=new Usuario();
        $usuario->nombre=$_POST["nombre"];
        $usuario->apellidoPaterno=$_POST["apellidoPaterno"];
        $usuario->apellidoMaterno=$_POST["apellidoMaterno"];
        $usuario->correo=$_POST["correo"];
        $usuario->contrasenia=$_POST["contrasenia"];
        $usuario->crear();

    }

    function vistalogin(){
        require "app/Views/vistaLogin.php";
    }

    function verificarLogin( ){
        if((!isset($_POST["correo"]) || (!isset( $_POST["contrasenia"])))){
            echo "Datos incorrectos";
            return false;
        }
        $correo=$_POST["correo"];
        $contrasenia=$_POST["contrasenia"];
        $verificar=Usuario::verificarUsuarios($correo,$contrasenia);

        if($verificar){
            require "app/Views/vistaPrincipal.php";
        }else{
            $estatus="DATOS INCORRECTOS";
            require "app/Views/vistaLogin.php";
        }

    }

}