<!doctype html>
    <html lang="en">
    <head>
         <meta charset="UTF-8">
         <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
         <meta http-equiv="X-UA-Compatible" content="ie=edge">
         <title>Mostrar</title>
    </head>
    <?php
    $pre = new PDO('mysql:host=localhost;dbname=inventario', 'root', '');
    ?>
    <body background="public/images/fondoGeneral.jpg">
    <center><p>TABLA DE INFORMACIÓN DE PRODCUTOS</p></center>

    <!--Tabla 3 que muestra los productos y muestra cuales caducan en 5 dias-->
     <table style="background-color: white" border=solid align="center">
         <tr>
             <td>ID PRODUCTO</td>
             <td>NOMBRE</td>
             <td>CANTIDAD</td>
             <td>COSTO</td>
             <td>CADUCIDAD</td>
             <td>VENCIMIENTO</td>
         </tr>
         <?php
            $con=mysqli_connect("localhost","root","","inventario") or die(mysqli_error());
            $query="SELECT * FROM productos";
            $result=mysqli_query($con,$query) or die (mysqli_error());
            $fechaActual= new DateTime(date('Y-m-d')); //variable de la fecha actual

            while ($row=mysqli_fetch_array($result)){
                echo "<tr>";
                echo "<td>".$row['id_producto']."</td>";
                echo "<td>".$row['nombre']."</td>";
                echo "<td>".$row['cantidad']."</td>";
                echo "<td>".$row['costo']."</td>";
                echo "<td>".$row['caducidad']."</td>";
                $fechabase= new DateTime($row['caducidad']);
                $dias=$fechaActual->diff($fechabase)->format('%r%a');

                //si la fecha vence en 5 dias
                if($dias>4 && $dias<=5){
                    echo '<td>Está a 5 días de vencer</td>';
                }
                //si l fecha es igual a la fecha actual
                if($dias<=0){
                    echo '<td>Prodcuto vencido</td>';
                }
                echo "</tr>";

            }
         ?>

     </table> <!--Fin de la tabla 1-->
     <br><br>

    <!--Tabla 2 que muestra productos que tienen menos de 5 en stock-->
    <table style="background-color: white" border=solid align="center">
        <tr>
            <td>ID PRODUCTO</td>
            <td>PRODUCTO CON MENOS DE 5 EN STOCK</td>
        </tr>

        <?php foreach ($pre->query('SELECT id_producto,nombre FROM productos WHERE cantidad<5')as $row){?>
            <tr>
                <td><?php echo $row['id_producto']?></td>
                <td><?php echo $row['nombre']?></td>
            </tr>

            <?php
        }
        ?>
    </table><br><br><!--Fin de la tabla 2-->
    <form action="index.php?controller=Principal&action=vistaPrincipal" method="POST" style="margin-left: 45%"  >
        <button style="background-color: #F30E29" onclick=sub()>Regresar</button>

    </form>


    </body>

    <style>
        p{
            color: #FFFFFF;
        }

        body{
            background-repeat : no-repeat;
            background-size:100% 100%;
            background-position: center;
            background-attachment: fixed;
            position: static;
            max-width: 100%;
        }

        /*pantalla tablets*/
        @media only screen and (min-width: 701px) and (max-width: 980px){
            .FORMU{
                width: 60%;
                margin: 0 auto;
            }
        }
        /*Smarthphones*/
        @media only screen and (min-width: 480px) and (max-width: 700px){
            .FORMU{
                width: 80%;
                margin: 0 auto;
            }
        }

    </style>
</html>
