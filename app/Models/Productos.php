<?php


namespace controlInventario;


class Productos extends Conexion
{
    public $idProducto;
    public $nombre;
    public $cantidad;
    public $costo;
    public $caducidad;

    public function __construct()
    {
        parent::__construct();//ejecutar el constructor del padre (conexion)
    }

    //registrar productos
    function registrar(){
        $pre=mysqli_prepare($this->con, "INSERT INTO productos(nombre,cantidad,costo,caducidad) VALUES (?,?,?,?)");
        $pre->bind_param("ssds", $this->nombre, $this->cantidad, $this->costo, $this->caducidad);
        $pre->execute();
        if($pre){
            $var = "REGISTRO  EXITOSO";
            echo "<script> alert('".$var."'); </script>";

        }
    }
    /*
    //verificar que existan los productos para actualizar
    static function verificarexistencia1(){
        $conexion=new Conexion();
        $pre=mysqli_prepare($conexion->con, "SELECT * FROM productos WHERE id_producto=?");
        $pre->bind_param("i", $idProducto);
        $pre->execute();
        $resultado= $pre->get_result();//obtiene resultado
        return  $resultado->fetch_object();//el resultado se pasa a objeto

    }*/

    //actualizar productos
    function actualizar(){
        $pre=mysqli_prepare($this->con, "UPDATE productos SET  cantidad=?,costo=? WHERE id_producto=?");
        $pre->bind_param("idi", $this->cantidad,$this->costo, $this->idProducto);
        $pre->execute();
        if($pre){
                $var="REGISTRO ACTUALIZADO";
                echo "<script> alert('".$var."') </script>";
        }
    }

    //verificar que existan los productos para eliminar
    static function verificarexistencia($nombre,$idProducto){
        $conexion=new Conexion();
        $pre=mysqli_prepare($conexion->con, "SELECT * FROM productos WHERE nombre=? AND id_producto=? ");
        $pre->bind_param("si", $nombre,$idProducto);
        $pre->execute();
        $resultado= $pre->get_result();//obtiene resultado
        return  $resultado->fetch_object();//el resultado se pasa a objeto

    }

    //eliminar productos
    function eliminar($nombre,$idProducto){
        $pre=mysqli_prepare($this->con, "DELETE FROM productos WHERE nombre=? AND id_producto=?");
        $pre->bind_param("si", $nombre,$idProducto);
        $pre->execute();
        if($pre){
            $var="PRODUCTO ELIMINADO";
            echo "<script> alert('".$var."')</script>";
        }
    }




}