<?php


namespace controlInventario;


class Usuario extends Conexion
{
    public $idUsuario;
    public $nombre;
    public $apellidoPaterno;
    public $apellidoMaterno;
    public $correo;
    public $contrasenia;

    public function __construct()
    {
        parent::__construct();//ejecutar el constructor del padre (conexion)
    }


    function crear(){
        $pre=mysqli_prepare($this->con, "INSERT INTO usuarios(nombre,apellido_paterno,apellido_Materno,correo,contrasenia) VALUES (?,?,?,?,?)");
        $pre->bind_param("sssss", $this->nombre, $this->apellidoPaterno, $this->apellidoMaterno,$this->correo, $this->contrasenia);
        $pre->execute();
        if($pre){
            $var = "REGISTRO  EXITOSO";
            echo "<script> alert('".$var."'); </script>";
            require "app/Views/vistaLogin.php";
        }
    }

    static function verificarUsuarios($correo,$contrasenia){ //funcion estatica
       $conexion=new Conexion();
       $pre=mysqli_prepare($conexion->con, "SELECT * FROM usuarios WHERE correo= ? AND contrasenia=?");
       $pre->bind_param("ss",$correo, $contrasenia);
       $pre->execute();
       $resultado= $pre->get_result();//obtiene resultado
        return  $resultado->fetch_object();//el resultado se pasa a objeto

    }


}