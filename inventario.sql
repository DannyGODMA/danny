-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-02-2021 a las 22:35:51
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `inventario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_producto` int(10) NOT NULL,
  `nombre` varchar(70) DEFAULT NULL,
  `cantidad` int(4) DEFAULT NULL,
  `costo` double DEFAULT NULL,
  `caducidad` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `nombre`, `cantidad`, `costo`, `caducidad`) VALUES
(9, 'chocolate', 1, 6, '2021-02-21'),
(10, 'cigarros', 100, 56, '2021-02-21'),
(11, 'donas', 4, 15, '2021-02-21'),
(12, 'Pan', 4, 35, '2021-02-26'),
(13, 'chocolate ferrero', 150, 6, '2021-02-27'),
(14, 'paleta payaso', 3, 15, '2021-02-28'),
(15, 'cacahuates', 200, 10, '2021-02-26'),
(16, 'mantequilla', 23, 11.5, '2021-02-27'),
(18, 'Refresco de manzana', 50, 500, '2022-06-16'),
(19, 'Dr pepper', 200, 17, '2021-02-27'),
(20, 'Leche lala', 50, 25, '2021-02-26'),
(21, 'Queso', 50, 60, '2021-02-27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(10) NOT NULL,
  `nombre` varchar(70) DEFAULT NULL,
  `apellido_paterno` varchar(70) DEFAULT NULL,
  `apellido_materno` varchar(70) DEFAULT NULL,
  `correo` varchar(70) DEFAULT NULL,
  `contrasenia` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre`, `apellido_paterno`, `apellido_materno`, `correo`, `contrasenia`) VALUES
(2, 'Jonathan Josué ', 'godinez', 'marquez', 'josuejosuebibi@gmail.com', '1234'),
(3, 'Juan Manuel', 'Perez', 'Mendoza', 'juan@gmail.com', '12345'),
(4, 'Angel David', 'Revilla', 'Lois', 'angel@gmail.com', '1234'),
(5, 'Daniela Noemí', 'godinez', 'marquez', 'daniela_1319104670@uptecamac.edu.mx', '123456789');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
